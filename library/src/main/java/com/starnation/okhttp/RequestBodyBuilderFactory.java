package com.starnation.okhttp;

import android.graphics.Bitmap;

import java.util.Locale;

import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/*
 * @author lsh
 * @since 2016. 5. 3.
*/
public final class RequestBodyBuilderFactory {

    //======================================================================
    // Constants
    //======================================================================

    private final static String FORMAT_JPG_MULTIPART = "%s.jpg";

    //======================================================================
    // Public Methods
    //======================================================================

    public static RequestBody createBitmapFileBody(Bitmap bitmap) throws Exception {
        return RequestBody.create(MultipartBody.FORM, Util.toCompressBitmapJPEG(bitmap, 100));
    }

    public static MultipartBody.Builder createMultipartBuilder(String key, Bitmap bitmap) throws Exception {
        return createMultipartBuilderInternal(MultipartBody.FORM, key, String.format(Locale.US, FORMAT_JPG_MULTIPART, Long.toString(System.currentTimeMillis())), bitmap);
    }

    public static RequestBody createFormBody(String key, String json) {
        return new FormBody.Builder().addEncoded(key, json).build();
    }

    public static MultipartBody.Builder createMultipartBuilder() {
        return new MultipartBody.Builder(Long.toString(System.currentTimeMillis()));
    }

    //======================================================================
    // Private Methods
    //======================================================================

    private static MultipartBody.Builder createMultipartBuilderInternal(MediaType mediaType, String key, String fileName, Bitmap bitmap) throws Exception {
        return new MultipartBody.Builder(Long.toString(System.currentTimeMillis()))
                .addFormDataPart(key, fileName, RequestBody.create(mediaType, Util.toCompressBitmapJPEG(bitmap, 100)));
    }
}
