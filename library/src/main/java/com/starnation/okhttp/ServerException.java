package com.starnation.okhttp;

import java.io.IOException;

/*
 * @author lsh
 * @since 2016. 5. 7.
*/
public class ServerException extends IOException {

    //======================================================================
    // Constants
    //======================================================================

    /**
     * 인증 에러
     */
    public final static int HTTP_STATUS_401 = 401;

    private final static int NOT_STATUS = -1;

    //======================================================================
    // Variables
    //======================================================================

    int mHttpStatusCode = NOT_STATUS;

    String mMessage;

    //======================================================================
    // Constructor
    //======================================================================

    public ServerException(String detailMessage) {
        super(detailMessage);
        mMessage = detailMessage;
    }

    //======================================================================
    // Public Methods
    //======================================================================

    @Override
    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public int getHttpStatusCode() {
        return mHttpStatusCode;
    }

    public void setHttpStatusCode(int httpStatusCode) {
        mHttpStatusCode = httpStatusCode;
    }
}
