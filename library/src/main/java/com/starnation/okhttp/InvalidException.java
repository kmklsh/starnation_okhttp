package com.starnation.okhttp;

/*
 * @author lsh
 * @since 2016. 2. 26.
*/
public class InvalidException extends Exception {

    public InvalidException(String detailMessage) {
        super(detailMessage);
    }
}
