package com.starnation.okhttp;


import java.io.IOException;

import okhttp3.Request;

/*
 * @author lsh
 * @since 2016. 12. 20.
*/
public interface Behavior<Response> {

    void onRequest(RequestBuilder<Response> requestBuilder);

    Response onSuccess(Response response);

    IOException onError(Request request, IOException ioe);

    void onComplete(boolean response, long responseTime);
}