package com.starnation.okhttp;

import android.annotation.SuppressLint;
import android.util.Log;

import java.util.Locale;

/*
 * @author lsh
 * @since 2016. 2. 25.
*/
final class LibraryLogger {

    private final static String TITLE = "########################### %s ###########################";

    private static boolean sLoggable;

    @SuppressLint("LogTagMismatch")
    public static void d(Tag tag, String message) {
        if (isLoggable(tag)) {
            try {
                Log.d(tag.toTag(), message);
            } catch (Exception e) {
                // Nothing
            }
        }
    }

    @SuppressLint("LogTagMismatch")
    public static void i(Tag tag, String message) {
        if (isLoggable(tag)) {
            try {
                Log.i(tag.toTag(), message);
            } catch (Exception e) {
                // Nothing
            }
        }
    }

    @SuppressLint("LogTagMismatch")
    public static void w(Tag tag, String message) {
        if (isLoggable(tag)) {
            try {
                Log.w(tag.toTag(), message);
            } catch (Exception e) {
                // Nothing
            }
        }
    }

    @SuppressLint("LogTagMismatch")
    public static void titleI(Tag tag, String message) {
        if (isLoggable(tag)) {
            try {
                Log.i(tag.toTag(), String.format(Locale.US, TITLE, message));
            } catch (Exception e) {
                // Nothing
            }
        }
    }

    @SuppressLint("LogTagMismatch")
    public static void e(Tag tag, String message) {
        if (isLoggable(tag)) {
            try {
                Log.e(tag.toTag(), message);
            } catch (Exception e) {
                // Nothing
            }
        }
    }

    @SuppressLint("LogTagMismatch")
    public static void titleE(Tag tag, String message) {
        if (isLoggable(tag)) {
            try {
                Log.e(tag.toTag(), String.format(Locale.US, TITLE, message));
            } catch (Exception e) {
                // Nothing
            }
        }
    }

    @SuppressLint("LogTagMismatch")
    public static void v(Tag tag, String message) {
        if (isLoggable(tag)) {
            try {
                Log.v(tag.toTag(), message);
            } catch (Exception e) {
                // Nothing
            }
        }
    }

    public static boolean isLoggable() {
        return sLoggable;
    }

    public static void setLoggable(boolean loggable) {
        sLoggable = loggable;
    }

    //======================================================================
    // Private Methods
    //======================================================================

    private static boolean isLoggable(Tag tag) {
        return sLoggable == true && tag.isEnable();
    }
}
