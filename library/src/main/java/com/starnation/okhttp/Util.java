package com.starnation.okhttp;

import android.graphics.Bitmap;

import java.io.ByteArrayOutputStream;

/*
 * @author lsh
 * @since 14. 8. 20.
*/
public class Util {

    public static boolean isEmpty(String text) {
        return text == null || text.trim().length() <= 0;
    }

    public static String textTrim(String text) {
        if (text != null && text.length() > 0) {
            return text.trim();
        } else {
            return "";
        }
    }

    public static boolean startsWith(String value, String prefix) {
        boolean result = false;
        value = textTrim(value);
        prefix = textTrim(prefix);
        if (!isEmpty(value)) {
            if (!isEmpty(prefix)) {
                result = value.startsWith(prefix);
            }
        }
        return result;
    }

    public static boolean isArrayValid(Object[] array, int index) {
        return index > -1 && array != null && array.length > index;
    }

    /**
     * Bitmap 이미지를 JPEG 형식으로 압축 해서 Byte 배열로 만든다.
     *
     * @param bitmap  bitmap
     * @param quality 0 ~ 100 숫자가 높을수록 압축 품질이 높아 진다.
     *
     * @return Byte 배열
     *
     * @throws Exception bitmap null 인경우 NullPointerException 발생
     */
    public static byte[] toCompressBitmapJPEG(Bitmap bitmap, int quality) throws Exception {
        return toCompressBitmap(bitmap, Bitmap.CompressFormat.JPEG, quality);
    }

    /**
     * Bitmap 이미지를 Bitmap.CompressFormat 에 맞게 압축 해서 Byte 배열로 만든다.
     *
     * @param bitmap         bitmap
     * @param compressFormat Bitmap.CompressFormat
     * @param quality        0 ~ 100 숫자가 높을수록 압축 품질이 높아 진다.
     *
     * @return Byte 배열
     *
     * @throws Exception bitmap null 인경우 NullPointerException 발생
     */
    public static byte[] toCompressBitmap(Bitmap bitmap, Bitmap.CompressFormat compressFormat, int quality) throws Exception {
        if (bitmap == null) {
            throw new NullPointerException("bitmap null");
        }
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(compressFormat, quality, outputStream);
        return outputStream.toByteArray();
    }
}
