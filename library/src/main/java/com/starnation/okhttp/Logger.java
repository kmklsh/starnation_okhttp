package com.starnation.okhttp;

/*
 * @author lsh
 * @since 2016. 12. 20.
*/
final class Logger {

    public static void log(String message) {
        LibraryLogger.i(Tag.OK_HTTP, message);
    }

    public static void logW(String message) {
        LibraryLogger.w(Tag.OK_HTTP, message);
    }

    public static void logV(String message) {
        LibraryLogger.v(Tag.OK_HTTP, message);
    }

    public static void logE(String message) {
        LibraryLogger.e(Tag.OK_HTTP, message);
    }

    public static void logTitle(String message) {
        LibraryLogger.titleI(Tag.OK_HTTP, message);
    }
}
