package com.starnation.okhttp;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;

/*
 * @author lsh
 * @since 15. 8. 11.
*/
public final class OkHttp {

    //======================================================================
    // Constants
    //======================================================================

    private final static long CONNECTION_TIME_OUT = 1000 * 30;

    private final static long READ_TIME_OUT = 1000 * 60;

    private final static long WRITE_TIME_OUT = 1000 * 60;

    final static String BODY_CONTENT = "application/x-www-form-urlencoded";

    //======================================================================
    // Variables
    //======================================================================

    private static OkHttpClient CLIENT;

    private static final List<JsonRequest> sJsonRequests = new CopyOnWriteArrayList<>();

    //======================================================================
    // Constructor
    //======================================================================

    static {
        CLIENT = new OkHttpClient.Builder()
                .connectTimeout(CONNECTION_TIME_OUT, TimeUnit.MILLISECONDS)
                .readTimeout(READ_TIME_OUT, TimeUnit.MILLISECONDS)
                .writeTimeout(READ_TIME_OUT, TimeUnit.MILLISECONDS)
                .retryOnConnectionFailure(true)
                .build();
    }

    public static void setLoggable(boolean log) {
        LibraryLogger.setLoggable(log);
    }

    public static RequestBuilder post() {
        return new RequestBuilder(Method.POST);
    }

    public static <Response> RequestBuilder<Response> get() {
        return new RequestBuilder<>(Method.GET);
    }

    public static RequestBuilder put() {
        return new RequestBuilder(Method.PUT);
    }

    public static <Response> RequestBuilder<Response> with(Method method) {
        return new RequestBuilder<>(method);
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public static OkHttpClient getClient() {
        if (CLIENT == null) {
            CLIENT = new OkHttpClient.Builder()
                    .connectTimeout(CONNECTION_TIME_OUT, TimeUnit.MILLISECONDS)
                    .readTimeout(READ_TIME_OUT, TimeUnit.MILLISECONDS)
                    .writeTimeout(READ_TIME_OUT, TimeUnit.MILLISECONDS)
                    .retryOnConnectionFailure(true)
                    .build();
        }
        return CLIENT;
    }

    public static void setClient(OkHttpClient client) {
        CLIENT = client;
    }

    public static void cancelAllRequest(int cancelKey) {
        for (JsonRequest request : sJsonRequests) {
            LibraryLogger.w(Tag.OK_HTTP, "cancelAllRequest -> cancelKey : " + cancelKey + " r cancelKey : " + request.getCancelKey());
            if (cancelKey == RequestBuilder.NO_CANCEL_KEY) {
                continue;
            }
            if (cancelKey == request.getCancelKey()) {
                request.cancel();
            }
        }
    }

    public static void clear() {
        sJsonRequests.clear();
    }

    public static void removeAllCancelRequest(int cancelKey) {
        for (JsonRequest request : sJsonRequests) {
            if (cancelKey == request.getCancelKey()) {
                sJsonRequests.remove(request);
            }
        }
    }

    //======================================================================
    // Methods
    //======================================================================

    static void add(JsonRequest request) {
        sJsonRequests.add(request);
        LibraryLogger.d(Tag.OK_HTTP, "add size : " + sJsonRequests.size() + " url : " + request.toString());
    }

    static void remove(JsonRequest dstRequest) {
        for (JsonRequest request : sJsonRequests) {
            if (request.getId() == dstRequest.getId()) {
                sJsonRequests.remove(request);
                LibraryLogger.d(Tag.OK_HTTP, "remove : " + request.toString());
            }
        }
        LibraryLogger.d(Tag.OK_HTTP, "remove size : " + sJsonRequests.size());
    }

    static boolean isRequestIn(JsonRequest dstRequest) {
        for (JsonRequest request : sJsonRequests) {
            if (request.getId() == dstRequest.getId()) {
                return true;
            }
        }
        return false;
    }
}
