package com.starnation.okhttp;

/*
 * @author lsh
 * @since 15. 6. 14.
*/
enum Tag {
    OK_HTTP;

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public String toString() {
        return super.toString();
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public String toTag() {
        return toString();
    }

    public boolean isEnable() {
        return true;
    }
}
