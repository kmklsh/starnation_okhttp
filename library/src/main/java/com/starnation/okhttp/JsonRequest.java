package com.starnation.okhttp;

import android.os.Handler;
import android.os.Looper;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Headers;
import okhttp3.Request;
import okhttp3.Response;

import static com.starnation.okhttp.Logger.log;
import static com.starnation.okhttp.Logger.logE;
import static com.starnation.okhttp.Logger.logTitle;
import static com.starnation.okhttp.Logger.logV;
import static com.starnation.okhttp.Logger.logW;

/*
 * @author lsh
 * @since 15. 8. 11.
 */
public class JsonRequest<R> {

    //=========================================================================
    // Constants
    //=========================================================================

    private final static String DEPENDING_NOTE = "&#034;";

    private final static String DEFAULT_CONTENT_CHARSET = "ISO-8859-1";

    private final static String CONTENT_TYPE = "Content-Type";

    //=========================================================================
    // Variables
    //=========================================================================

    private final RequestBuilder mRequestBuilder;

    private boolean mCancel;

    private long mCurrentTime;

    private long mResponseTime;

    private final boolean mDebug;

    private boolean mResponse;

    private Behavior mBehavior;

    private OnResponseListener mResponseListener;

    private OnStatusListener mOnStatusListener;

    private OnCompleteListener mCompleteListener;

    private Parse mParse;

    //======================================================================
    // Constructor
    //======================================================================

    @SuppressWarnings("unchecked")
    public JsonRequest(RequestBuilder requestBuilder) {
        mRequestBuilder = requestBuilder;
        mDebug = requestBuilder.isLogEnable();
        mBehavior = requestBuilder.mBehavior;
        mParse = requestBuilder.mParse;
    }

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public String toString() {
        return mRequestBuilder.toString();
    }

    //======================================================================
    // Public Methods
    //======================================================================

    @SuppressWarnings("unchecked")
    public void cancel() {
        mCancel = true;
        if (mDebug == true) {
            logW("cancel url : " + mRequestBuilder.getSuffixUrl() + " cancelKey : " + mRequestBuilder.mCancelKey);
        }
        complete();
    }

    int getCancelKey() {
        return mRequestBuilder.mCancelKey;
    }

    public void immediateExecute() {
        try {
            executeInternal(true);
        } catch (Exception e) {
            logE(e.getMessage());
        }
    }

    @SuppressWarnings("TypeParameterHidesVisibleType")
    public void execute(OnResponseListener<R> responseListener) {
        try {
            responseListener(responseListener);
            executeInternal(false);
        } catch (IOException e) {
            logE(e.getMessage());
        }
    }

    public void execute() {
        try {
            executeInternal(false);
        } catch (IOException e) {
            logE(e.getMessage());
        }
    }

    @SuppressWarnings("TypeParameterHidesVisibleType")
    public JsonRequest<R> responseListener(JsonRequest.OnResponseListener<R> responseListener) {
        mResponseListener = responseListener;
        return this;
    }

    public JsonRequest<R> statusListener(JsonRequest.OnStatusListener onStatusListener) {
        mOnStatusListener = onStatusListener;
        return this;
    }

    public JsonRequest<R> completeListener(OnCompleteListener completeListener) {
        mCompleteListener = completeListener;
        return this;
    }

    //======================================================================
    // Methods
    //======================================================================

    int getId() {
        return mRequestBuilder.getId();
    }

    //======================================================================
    // Methods
    //======================================================================

    private Request buildOKHTTPRequest() {
        try {
            mRequestBuilder.valid();
        } catch (InvalidException e) {
            e.printStackTrace();
        }

        Request.Builder builder = null;
        switch(mRequestBuilder.mMethod) {
            case PUT:
                builder = new Request.Builder().put(mRequestBuilder.getRequestBody());
                break;
            case POST:
                builder = new Request.Builder().post(mRequestBuilder.getRequestBody());
                break;
            case DELETE:
                builder = new Request.Builder().delete(mRequestBuilder.getRequestBody());
                break;
            case GET:
                builder = new Request.Builder().get();
                break;
        }
        if (builder != null) {
            return builder.url(mRequestBuilder.getSuffixUrl())
                    .headers(mRequestBuilder.asHeaders())
                    .tag(mRequestBuilder.mCancelKey)
                    .build();
        }
        return null;
    }

    //======================================================================
    // Private Methods
    //======================================================================

    @SuppressWarnings("unchecked")
    private void executeInternal(boolean immediateExecute) throws IOException {
        if (mRequestBuilder.isDuplicateRequest() == false && OkHttp.isRequestIn(this) == true) {
            throw new IOException("Reqeust In -> Info : " + toString());
        }

        if (immediateExecute == true) {
            if (Looper.myLooper() == Looper.getMainLooper()) {
                throw new IOException("Not UI Thread");
            }
        }

        if (mBehavior != null) {
            mBehavior.onRequest(mRequestBuilder);
        }

        Request request = buildOKHTTPRequest();
        if (request == null) {
            throw new IOException("com.squareup.okhttp.Request null");
        }

        if (mOnStatusListener != null) {
            mOnStatusListener.onNetworkStart();
        }

        Call call = OkHttp.getClient().newCall(request);
        Callback callback = new Callback();

        if (immediateExecute == true) {
            try {
                Response response = call.execute();
                callback.onResponse(call, response);
            } catch (IOException e) {
                callback.onFailure(call, e);
            }
        } else {
            call.enqueue(callback);
        }

        OkHttp.add(this);

        mCurrentTime = System.currentTimeMillis();
    }

    private void complete() {
        if (mBehavior != null) {
            mBehavior.onComplete(mResponse, mResponseTime);
        }

        if (mCompleteListener != null) {
            mCompleteListener.complete(mResponse, mResponseTime);
        }
        if (mDebug == true) {
            log("complete url : " + mRequestBuilder.getSuffixUrl());
        }
        OkHttp.removeAllCancelRequest(mRequestBuilder.mCancelKey);
        OkHttp.remove(this);
        mRequestBuilder.release();
        mCurrentTime = 0;
        mResponseTime = 0;
        mResponse = false;
        mCompleteListener = null;
        mResponseListener = null;
        mOnStatusListener = null;
        mParse = null;
        mBehavior = null;
    }

    //======================================================================
    // BehaviorInner
    //======================================================================

    final class Callback implements okhttp3.Callback {

        final Handler mHandler = new Handler(Looper.getMainLooper());

        //======================================================================
        // Override Methods
        //======================================================================

        @Override
        public void onFailure(Call call, IOException ioe) {
            if (mCancel == true) {
                complete();
                return;
            }
            Request request = call.request();

            mResponse = false;
            if (mDebug == true) {
                LibraryLogger.titleE(Tag.OK_HTTP, "onFailure");
                logE("method : " + request.method() + " url : " + request.url().url().toString());
                logE("IOException : " + ioe.getClass().getSimpleName());
                logE("message : " + ioe.getMessage());
            }
            deliverFailure(request, ioe);
        }

        @SuppressWarnings("unchecked")
        @Override
        public void onResponse(Call call, Response response) throws IOException {
            if (mCancel == true) {
                complete();
                return;
            }

            mResponse = true;
            if (mDebug == true) {
                mResponseTime = (System.currentTimeMillis() - mCurrentTime);
                logTitle("onResponse");
                log("http statusCode : " + response.code() + " successFull : " + response.isSuccessful() + " redirect : " + response.isRedirect());
                log("url : " + response.request().url().url().toString() + " response time : " + mResponseTime);
                log("message : " + response.message());
            }
            try {
                /*if (response.isSuccessful() == false) {
                    ServerException serverException = new ServerException("Sever or Network Error http status code : " + response.code() + " " + response.message());
                    serverException.setHttpStatusCode(response.code());
                    throw serverException;
                }*/

                String jsonString = parseResponseBody(mDebug, response);
                Object result;
                if (mParse != null) {
                    result = mParse.parse(response, jsonString);
                } else {
                    result = jsonString;
                }
                deliverResponse(result);
            } catch (IOException e) {
                onFailure(call, e);
            } catch (Exception e) {
                onFailure(call, new IOException(e.getMessage()));
            }
        }

        //======================================================================
        // Private Methods
        //======================================================================

        String parseResponseBody(boolean printLog, Response response) throws IOException {
            String jsonString = new String(response.body().bytes(), parseCharset(response.headers()));
            if (printLog == true) {
                logV("jsonString : " + jsonString);
            }
            if (Util.isEmpty(jsonString) == true) {
                throw new IOException("response result null");
            }

            if (jsonString.contains(DEPENDING_NOTE) == true) {
                jsonString = jsonString.replaceAll(DEPENDING_NOTE, "\"");
            }
            return jsonString;
        }

        String parseCharset(Headers headers) {
            String contentType = headers.get(CONTENT_TYPE);
            if (contentType != null) {
                String[] params = contentType.split(";");
                for (int i = 1; i < params.length; i++) {
                    String[] pair = params[i].trim().split("=");
                    if (pair.length == 2) {
                        if (pair[0].equals("charset")) {
                            return pair[1];
                        }
                    }
                }
            }
            return DEFAULT_CONTENT_CHARSET;
        }

        @SuppressWarnings("unchecked")
        private boolean deliverResponse(final Object result) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    try {
                        Object resultResponse = mBehavior != null ? mBehavior.onSuccess(result) : result;

                        if (mOnStatusListener != null) {
                            mOnStatusListener.onNetworkEnd();
                        }
                        if (mResponseListener != null) {
                            mResponseListener.onSuccess(resultResponse);
                        }
                        complete();
                    } catch (Exception e) {
                        logE(e.getMessage());
                        complete();
                    }
                    mHandler.removeCallbacks(this);
                }
            });
            return true;
        }

        private boolean deliverFailure(final Request request, final IOException ioe) {
            mHandler.post(new Runnable() {
                @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
                @Override
                public void run() {
                    try {
                        if (mOnStatusListener != null) {
                            mOnStatusListener.onNetworkEnd();
                        }
                        IOException resultIoe = mBehavior != null ? mBehavior.onError(request, ioe) : ioe;
                        if (mResponseListener != null) {
                            mResponseListener.onError(request, resultIoe);
                        }
                        complete();
                    } catch (Exception e) {
                        logE(e.getMessage());
                        complete();
                    }
                    mHandler.removeCallbacks(this);
                }
            });
            return true;
        }
    }

    //======================================================================
    // OnResponseListener
    //======================================================================

    public interface OnResponseListener<Response> {
        void onSuccess(Response response);

        void onError(Request request, IOException ioe);
    }

    //======================================================================
    // OnStatusListener
    //======================================================================

    public interface OnStatusListener {

        void onNetworkStart();

        void onNetworkEnd();
    }

    //======================================================================
    // Parse
    //======================================================================

    public interface Parse<Result> {
        Result parse(Response response, String json) throws IOException;
    }

    //======================================================================
    // OnCompleteListener
    //======================================================================

    public interface OnCompleteListener {
        void complete(boolean response, long responseTime);
    }
}
