package com.starnation.okhttp;

import android.os.Handler;
import android.os.Looper;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executors;

/*
 * @author lsh
 * @since 2018. 1. 10.
*/
public class JsonRequests {

    //======================================================================
    // Variables
    //======================================================================

    private List<JsonRequest> mJsonRequests = new CopyOnWriteArrayList<>();

    private final int mThreadPool;

    private JsonRequest.OnCompleteListener mOnCompleteListener;

    private Handler mHandler = new Handler(Looper.getMainLooper());

    //======================================================================
    // Constructor
    //======================================================================

    public static JsonRequests with(int threadPool) {
        return new JsonRequests(threadPool);
    }

    public JsonRequests(int threadPool) {
        mThreadPool = threadPool;
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public JsonRequests completeListener(JsonRequest.OnCompleteListener completeListener) {
        mOnCompleteListener = completeListener;
        return this;
    }

    public JsonRequests next(JsonRequest request) {
        mJsonRequests.add(request);
        return this;
    }

    public void execute() {
        final long startTime = System.currentTimeMillis();
        Executors.newFixedThreadPool(mThreadPool).execute(new Runnable() {
            @Override
            public void run() {
                for (JsonRequest request : mJsonRequests) {
                    request.immediateExecute();
                }
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (mOnCompleteListener != null){
                            mOnCompleteListener.complete(true, System.currentTimeMillis() - startTime);
                        }
                    }
                });
            }
        });
    }
}
