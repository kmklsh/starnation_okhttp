package com.starnation.okhttp;

/*
 * @author lsh
 * @since 2016. 5. 2.
*/
public enum  Method {
    POST,
    GET,
    PUT,
    DELETE,
}
