package com.starnation.okhttp;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Locale;
import java.util.Map;

import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.RequestBody;

/*
 * @author lsh
 * @since 15. 8. 17.
*/
public class RequestBuilder<Response> {

    //======================================================================
    // Constants
    //======================================================================

    private final static String REQUEST_INFO_FORMAT = "url: %s, header: %s, body: %s";

    private final static Headers EMPTY_HEADERS = new Headers.Builder().build();

    public final static int NO_CANCEL_KEY = -1;

    //=========================================================================
    // Variables
    //=========================================================================

    Map<String, String> mHeader;

    String mUrl;

    String mSuffix;

    String mUserAgent;

    RequestBody mRequestBody = RequestBody.create(MediaType.parse(OkHttp.BODY_CONTENT), OkHttp.BODY_CONTENT);

    Method mMethod = Method.GET;

    boolean mLogEnable = LibraryLogger.isLoggable();

    boolean mDuplicateRequest = true;

    int mCancelKey = NO_CANCEL_KEY;

    Behavior<Response> mBehavior;

    JsonRequest.Parse<Response> mParse;

    //======================================================================
    // Constructor
    //======================================================================

    public RequestBuilder(Method method) {
        mMethod = method;
    }

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public String toString() {
        return String.format(Locale.US, REQUEST_INFO_FORMAT, getSuffixUrl(), adHeaderToString(), asBodyToString());
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public int getId() {
        return toString().hashCode();
    }

    public Method getMethod() {
        return mMethod;
    }

    public String getSuffixUrl() {
        if (Util.isEmpty(mSuffix) == false) {
            String suffix = mSuffix;
            if (mSuffix.startsWith(File.separator) == false
                    && mUrl.endsWith(File.separator) == false) {
                suffix = File.separator + mSuffix;
            }
            return mUrl + suffix;
        }
        return mUrl;
    }

    public String getUrl() {
        return mUrl;
    }

    @SuppressWarnings("unchecked")
    public RequestBuilder<Response> url(String url) {
        mUrl = url;
        return this;
    }

    /**
     * 페이지 접미사로 붙는경우 사용
     *
     * @param suffix 접미사 ex) 1234, 1234/1245/122
     */
    @SuppressWarnings("unchecked")
    public RequestBuilder<Response> suffix(String suffix) {
        this.mSuffix = suffix;
        return this;
    }

    public Map<String, String> getHeader() {
        return mHeader;
    }

    @SuppressWarnings("unchecked")
    public RequestBuilder<Response> header(Map<String, String> header) {
        mHeader = header;
        return this;
    }

    public String getUserAgent() {
        return mUserAgent;
    }

    @SuppressWarnings("unchecked")
    public RequestBuilder<Response> userAgent(String userAgent) {
        mUserAgent = userAgent;
        return this;
    }

    public RequestBody getRequestBody() {
        return mRequestBody;
    }

    @SuppressWarnings("unchecked")
    public RequestBuilder<Response> requestBody(RequestBody requestBody) {
        mRequestBody = requestBody;
        return this;
    }

    @SuppressWarnings("unchecked")
    public RequestBuilder<Response> logEnable(boolean logEnable) {
        mLogEnable = logEnable;
        return this;
    }

    public boolean isDuplicateRequest() {
        return mDuplicateRequest;
    }

    @SuppressWarnings("unchecked")
    public RequestBuilder<Response> duplicateRequest(boolean duplicateRequest) {
        mDuplicateRequest = duplicateRequest;
        return this;
    }

    @SuppressWarnings("unchecked")
    public RequestBuilder<Response> cancelKey(int key) {
        mCancelKey = key;
        return this;
    }

    @SuppressWarnings("unchecked")
    public RequestBuilder<Response> behavior(Behavior behavior) {
        mBehavior = behavior;
        return this;
    }

    @SuppressWarnings("unchecked")
    public RequestBuilder<Response> parse(JsonRequest.Parse parse) {
        mParse = parse;
        return this;
    }

    public JsonRequest<Response> statusListener(JsonRequest.OnStatusListener listener) {
        return new JsonRequest<Response>(this).statusListener(listener);
    }

    public JsonRequest<Response> completeListener(JsonRequest.OnCompleteListener listener) {
        return new JsonRequest<Response>(this).completeListener(listener);
    }

    public JsonRequest<Response> responseListener(JsonRequest.OnResponseListener<Response> listener) {
        return new JsonRequest<Response>(this).responseListener(listener);
    }

    public void execute(JsonRequest.OnResponseListener<Response> listener) {
        new JsonRequest<Response>(this).execute(listener);
    }

    public JsonRequest<Response> build() {
        return new JsonRequest<Response>(this);
    }

    public void release() {
        mHeader = null;
        mRequestBody = null;
    }

    public Headers asHeaders() {
        if (mHeader != null) {
            Headers.Builder builder = new Headers.Builder();
            for (Map.Entry<String, String> entry : mHeader.entrySet()) {
                builder.add(entry.getKey(), entry.getValue());
            }
            return builder.build();
        }
        return EMPTY_HEADERS;
    }

    public String adHeaderToString() {
        if (mHeader != null) {
            StringBuilder builder = new StringBuilder();
            for (Map.Entry<String, String> entry : mHeader.entrySet()) {

                String value;
                try {
                    value = URLDecoder.decode(entry.getValue(), "utf-8");
                } catch (UnsupportedEncodingException e) {
                    value = entry.getValue();
                }
                builder.append("key:").append(entry.getKey()).append(",value:").append(value);
            }
            return builder.toString();
        }
        return "";
    }

    public String asBodyToString() {
        if (mRequestBody != null) {
            long length = 0;
            try {
                length = mRequestBody.contentLength();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return mRequestBody.contentType() + ":" + length;
        }
        return "";
    }

    public boolean isLogEnable() {
        return LibraryLogger.isLoggable() && mLogEnable;
    }

    //======================================================================
    // Methods
    //======================================================================

    void valid() throws InvalidException {
        if (Util.isEmpty(mUrl) == true) {
            throw new InvalidException("Url Null");
        }
    }
}
