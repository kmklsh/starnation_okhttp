package com.starnation.okhttp.annotation;

import com.starnation.okhttp.Behavior;
import com.starnation.okhttp.JsonRequest;
import com.starnation.okhttp.Method;
import com.starnation.okhttp.OkHttp;
import com.starnation.okhttp.RequestBuilder;

import java.io.IOException;

/*
 * @author vkwofm
 * @since 2017. 8. 16.
*/
public class RequestBuilderCall<T> implements Call<T> {

    //======================================================================
    // Variables
    //======================================================================

    String url;

    Method mMethod;

    JsonRequest.Parse mParse;

    Behavior mBehavior;

    //======================================================================
    // Constructor
    //======================================================================

    public RequestBuilderCall(String url, Method method, JsonRequest.Parse parse, Behavior behavior) {
        this.url = url;
        mMethod = method;
        mParse = parse;
        mBehavior = behavior;
    }

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public T execute() throws IOException {
        return null;
    }

    @Override
    public RequestBuilder<T> request() {
        return OkHttp.<T>with(mMethod)
                .url(url)
                .parse(mParse)
                .behavior(mBehavior);
    }
}
