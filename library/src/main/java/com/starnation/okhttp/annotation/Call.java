package com.starnation.okhttp.annotation;

import com.starnation.okhttp.RequestBuilder;

import java.io.IOException;

/*
 * @author vkwofm
 * @since 2017. 8. 16.
*/
public interface Call<T> {

    @Deprecated
    T execute() throws IOException;
    RequestBuilder<T> request();
}
