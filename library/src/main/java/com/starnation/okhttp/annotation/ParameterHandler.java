package com.starnation.okhttp.annotation;

import java.io.IOException;

/*
 * @author vkwofm
 * @since 2017. 8. 17.
*/
abstract class ParameterHandler<T> {
    abstract void apply(ServiceMethod.Builder builder, T value) throws IOException;

    //======================================================================
    // InnerClass
    //======================================================================

    static final class Url<T> extends ParameterHandler<T> {
        private final String mName;

        public Url(String name) {
            mName = name;
        }

        @Override
        void apply(ServiceMethod.Builder builder, T value) throws IOException {
            if (value == null) {
                throw new IllegalArgumentException("Url parameter \"" + mName + "\" value must not be null.");
            }
            builder.mUrlFactory.mBaseUrl = "";
            builder.mUrlFactory.mDomainSuffix = "";
            builder.mUrlFactory.mRelativeUrl = value.toString();
        }
    }

    static final class Path<T> extends ParameterHandler<T> {
        private final String mName;
        private final boolean mEncoded;

        public Path(String name, boolean encoded) {
            mName = name;
            mEncoded = encoded;
        }

        @Override
        void apply(ServiceMethod.Builder builder, T value) throws IOException {
            if (value == null) {
                throw new IllegalArgumentException("Path parameter \"" + mName + "\" value must not be null.");
            }
            builder.mUrlFactory.addPathParam(mName, value.toString(), mEncoded);
        }
    }

    static final class Query<T> extends ParameterHandler<T> {
        private final String mName;
        private final boolean mEncoded;

        public Query(String name, boolean encoded) {
            mName = name;
            mEncoded = encoded;
        }

        @Override
        void apply(ServiceMethod.Builder builder, T value) throws IOException {
            if (value == null) {
                throw new IllegalArgumentException("Query parameter \"" + mName + "\" value must not be null.");
            }
            builder.mUrlFactory.addQueryParam(mName, value.toString(), mEncoded);
        }
    }

    static final class Behavior<T> extends ParameterHandler<T> {
        private final String mName;

        public Behavior(String name) {
            mName = name;
        }

        @Override
        void apply(ServiceMethod.Builder builder, T value) throws IOException {
            if (value == null) {
                throw new IllegalArgumentException("Behavior parameter \"" + mName + "\" value must not be null.");
            }
            builder.mBehavior = (com.starnation.okhttp.Behavior) value;
        }
    }
}
