package com.starnation.okhttp.annotation;

import com.starnation.okhttp.Behavior;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

/*
 * @author vkwofm
 * @since 2017. 8. 16.
*/
public class RequestFactory {

    //======================================================================
    // Variables
    //======================================================================

    private String mBaseUrl;

    private String mDomainSuffix;

    private RequestParse mRequestParse;

    private com.starnation.okhttp.Behavior mBehavior;

    //======================================================================
    // Public Methods
    //======================================================================

    public RequestFactory baseUrl(String baseUrl) {
        mBaseUrl = baseUrl;
        return this;
    }

    public String getBaseUrl() {
        return mBaseUrl;
    }

    public RequestFactory domainSuffix(String domainSuffix) {
        mDomainSuffix = domainSuffix;
        return this;
    }

    public String getDomainSuffix() {
        return mDomainSuffix;
    }

    public <T> T create(final Class<T> service) {
        return (T) Proxy.newProxyInstance(service.getClassLoader(), new Class<?>[] { service },
                new InvocationHandler() {
                    @Override
                    public Object invoke(Object proxy, java.lang.reflect.Method method, Object[] args) throws Throwable {
                        try {
                            // If the method is a method from Object then defer to normal invocation.
                            if (method.getDeclaringClass() == Object.class) {
                                throw new Exception();
                            }

                            return new ServiceMethod.Builder<>(RequestFactory.this, method, args)
                                    .build()
                                    .method();
                        } catch (Exception e) {
                            return method.invoke(this, args);
                        }
                    }
                });
    }

    //======================================================================
    // Protected Methods
    //======================================================================

    protected void setRequestParse(RequestParse requestParse) {
        mRequestParse = requestParse;
    }

    protected void setBehavior(Behavior behavior) {
        mBehavior = behavior;
    }

    //======================================================================
    // Methods
    //======================================================================

    com.starnation.okhttp.Behavior getBehavior() {
        return mBehavior;
    }

    RequestParse getRequestParse() {
        return mRequestParse;
    }
}
