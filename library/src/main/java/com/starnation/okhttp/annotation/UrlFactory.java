package com.starnation.okhttp.annotation;

import com.starnation.okhttp.Util;

import java.util.HashMap;
import java.util.Map;

import okio.Buffer;

/*
 * @author vkwofm
 * @since 2017. 8. 17.
*/
final class UrlFactory {

    //======================================================================
    // Constance
    //======================================================================

    private static final char[] HEX_DIGITS =
            { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

    private static final String PATH_SEGMENT_ALWAYS_ENCODE_SET = " \"<>^`{}|\\?#";

    //======================================================================
    // Variables
    //======================================================================

    String mBaseUrl;

    String mDomainSuffix = "";

    String mRelativeUrl;

    Map<String, String> mUrlParam = new HashMap<>();

    //======================================================================
    // Constructor
    //======================================================================

    UrlFactory(String baseUrl, String relativeUrl) {
        mBaseUrl = baseUrl;
        mRelativeUrl = relativeUrl;
    }

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public String toString() {
        String url = mBaseUrl;
        url = addUrlInternal(url, mDomainSuffix);
        url = addUrlInternal(url, mRelativeUrl);

        if (mUrlParam.size() > 0) {
            url += toUrlParam();
        }

        return url;
    }

    //======================================================================
    // Protected Methods
    //======================================================================

    protected UrlFactory setDomainSuffix(String domainSuffix) {
        mDomainSuffix = domainSuffix == null ? "" : domainSuffix;
        return this;
    }

    protected String toUrlParam() {
        String urlParam = "?";
        urlParam = "?";
        for (String key : mUrlParam.keySet()) {
            if (urlParam.equals("?") != true) {
                urlParam += "&";
            }
            urlParam += key + "=" + mUrlParam.get(key);
        }
        return urlParam;
    }

    //======================================================================
    // Method
    //======================================================================

    void addPathParam(String name, String value, boolean encoded) {
        if (mRelativeUrl == null) {
            throw new AssertionError();
        }
        if (mRelativeUrl.contains("/:") == true) {
            mRelativeUrl = mRelativeUrl.replace("/:" + name, "/" + canonicalizeForPath(value, encoded));
        } else {
            mRelativeUrl = mRelativeUrl.replace("{" + name + "}", canonicalizeForPath(value, encoded));
        }
    }

    void addQueryParam(String name, String value, boolean encoded) {
        if (mRelativeUrl == null) {
            throw new AssertionError();
        }
        mUrlParam.put(name, canonicalizeForPath(value, encoded));
    }

    //======================================================================
    // Private Methods
    //======================================================================

    private static String canonicalizeForPath(String input, boolean alreadyEncoded) {
        int codePoint;
        for (int i = 0, limit = input.length(); i < limit; i += Character.charCount(codePoint)) {
            codePoint = input.codePointAt(i);
            if (codePoint < 0x20 || codePoint >= 0x7f
                    || PATH_SEGMENT_ALWAYS_ENCODE_SET.indexOf(codePoint) != -1
                    || (!alreadyEncoded && (codePoint == '/' || codePoint == '%'))) {
                // Slow path: the character at i requires encoding!
                Buffer out = new Buffer();
                out.writeUtf8(input, 0, i);
                canonicalizeForPath(out, input, i, limit, alreadyEncoded);
                return out.readUtf8();
            }
        }

        // Fast path: no characters required encoding.
        return input;
    }

    private static void canonicalizeForPath(Buffer out, String input, int pos, int limit, boolean alreadyEncoded) {
        Buffer utf8Buffer = null; // Lazily allocated.
        int codePoint;
        for (int i = pos; i < limit; i += Character.charCount(codePoint)) {
            codePoint = input.codePointAt(i);
            if (alreadyEncoded
                    && (codePoint == '\t' || codePoint == '\n' || codePoint == '\f' || codePoint == '\r')) {
                // Skip this character.
            } else if (codePoint < 0x20 || codePoint >= 0x7f
                    || PATH_SEGMENT_ALWAYS_ENCODE_SET.indexOf(codePoint) != -1
                    || (!alreadyEncoded && (codePoint == '/' || codePoint == '%'))) {
                // Percent encode this character.
                if (utf8Buffer == null) {
                    utf8Buffer = new Buffer();
                }
                utf8Buffer.writeUtf8CodePoint(codePoint);
                while (!utf8Buffer.exhausted()) {
                    int b = utf8Buffer.readByte() & 0xff;
                    out.writeByte('%');
                    out.writeByte(HEX_DIGITS[(b >> 4) & 0xf]);
                    out.writeByte(HEX_DIGITS[b & 0xf]);
                }
            } else {
                // This character doesn't need encoding. Just copy it over.
                out.writeUtf8CodePoint(codePoint);
            }
        }
    }

    private String addUrlInternal(String base, String extension) {
        String newBase = removeStartEndSlush(base);
        String newExtension = removeStartEndSlush(extension);
        return newBase
                + ((Util.isEmpty(newBase) == false && Util.isEmpty(newBase) == false) ? "/" : "")
                + newExtension;
    }

    private String removeStartEndSlush(String src) {
        try {
            src = src.endsWith("/") == true ? src.substring(0, src.length() - 1) : src;
            src = src.startsWith("/") == true ? src.substring(1) : src;
            return src;
        } catch (Exception e) {
            return Util.isEmpty(src) == true ? "" : src;
        }
    }
}
