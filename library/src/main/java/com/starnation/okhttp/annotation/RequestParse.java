package com.starnation.okhttp.annotation;

import com.starnation.okhttp.JsonRequest;

import java.lang.reflect.Type;

/*
 * @author vkwofm
 * @since 2017. 8. 21.
*/
public abstract class RequestParse {

    //======================================================================
    // Public Methods
    //======================================================================

    protected abstract JsonRequest.Parse<?> toParseType(Type type);
}
