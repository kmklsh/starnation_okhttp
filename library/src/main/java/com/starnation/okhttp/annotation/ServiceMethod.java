package com.starnation.okhttp.annotation;

import com.starnation.okhttp.JsonRequest;
import com.starnation.okhttp.annotation.model.Query;
import com.starnation.okhttp.annotation.model.RequestBehavior;
import com.starnation.okhttp.annotation.model.Path;
import com.starnation.okhttp.annotation.model.RequestGet;
import com.starnation.okhttp.annotation.model.RequestPost;
import com.starnation.okhttp.annotation.model.Url;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.ArrayList;

import static com.starnation.okhttp.Method.GET;
import static com.starnation.okhttp.Method.POST;

/*
 * @author vkwofm
 * @since 2017. 8. 16.
*/
public class ServiceMethod<T> {

    //======================================================================
    // Variables
    //======================================================================

    Builder mBuilder;

    //======================================================================
    // Construcotr
    //======================================================================

    public ServiceMethod(Builder builder) {
        mBuilder = builder;
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public Call<T> method() {
        return new RequestBuilderCall<T>(mBuilder.mUrlFactory.toString(),
                mBuilder.mHttpMethod,
                mBuilder.mRequestParse,
                mBuilder.mBehavior);
    }

    //======================================================================
    // Builder
    //======================================================================

    public static class Builder<T> {

        //======================================================================
        // Variables
        //======================================================================

        protected Method mMethod;

        protected String mUrl;

        protected String mDomainSuffix;

        protected UrlFactory mUrlFactory;

        protected com.starnation.okhttp.Method mHttpMethod;

        protected com.starnation.okhttp.Behavior mBehavior;

        protected JsonRequest.Parse mRequestParse;

        protected Type mReturnType;

        ArrayList<ParameterHandler<?>> mParameterHandlerList = new ArrayList<>();

        //======================================================================
        // Constructor
        //======================================================================

        public Builder(RequestFactory factory, Method method, Object[] args) throws IOException{
            mMethod = method;
            mUrl = factory.getBaseUrl();
            mDomainSuffix = factory.getDomainSuffix();
            mReturnType =  ServiceMethodHelper.getActualTypeArguments(method, 0);
            mRequestParse = factory.getRequestParse().toParseType(mReturnType);
            mBehavior = factory.getBehavior();

            parseMethodAnnotation();
            parseParameterAnnotation(args);
        }

        //======================================================================
        // Public Methods
        //======================================================================

        public ServiceMethod<T> build() {
            return new ServiceMethod(this);
        }

        //======================================================================
        // Protected Methods
        //======================================================================

        protected Type getCallResponseType() {
            return ServiceMethodHelper.getCallResponseType(mReturnType);
        }

        //======================================================================
        // Private Methods
        //======================================================================

        private void parseParameterAnnotation(Object[] args) throws IOException{
            if (mMethod.getParameterAnnotations() != null) {
                for (Annotation[] annotations : mMethod.getParameterAnnotations()) {
                    for (Annotation annotation : annotations) {
                        parseParameterAnnotationInternal(annotation);
                    }
                }
            }

            for (int index = 0; index < mParameterHandlerList.size(); index++) {
                ParameterHandler<Object> parameterHandler = (ParameterHandler<Object>) mParameterHandlerList.get(index);
                parameterHandler.apply(this, args[index]);
            }
        }

        private void parseParameterAnnotationInternal(Annotation annotation) {
            if (annotation instanceof Url) {
                if (mUrlFactory != null) {
                    Url url = (Url) annotation;
                    mParameterHandlerList.add(new ParameterHandler.Url<>(url.value()));
                }
            } else if (annotation instanceof Query) {
                if (mUrlFactory != null) {
                    Query query = (Query) annotation;
                    mParameterHandlerList.add(new ParameterHandler.Query<>(query.value(), query.encoded()));
                }
            } else if (annotation instanceof Path) {
                if (mUrlFactory != null) {
                    Path path = (Path) annotation;
                    mParameterHandlerList.add(new ParameterHandler.Path<>(path.value(), path.encoded()));
                }
            } else if (annotation instanceof RequestBehavior) {
                RequestBehavior requestBehavior = (RequestBehavior) annotation;
                mParameterHandlerList.add(new ParameterHandler.Behavior<>(requestBehavior.toString()));
            }
        }

        private void parseMethodAnnotation() {
            if (mMethod.getAnnotations() != null) {
                for (Annotation annotation : mMethod.getAnnotations()) {
                    if (annotation instanceof RequestGet) {
                        setMethodAnnotation(GET, ((RequestGet) annotation).value());
                    } else if (annotation instanceof RequestPost) {
                        setMethodAnnotation(POST, ((RequestPost) annotation).value());
                    }
                }
            }
        }

        private void setMethodAnnotation(com.starnation.okhttp.Method httpMethod, String value) {
            mHttpMethod = httpMethod;
            mUrlFactory = new UrlFactory(mUrl, value).setDomainSuffix(mDomainSuffix);
        }
    }
}
